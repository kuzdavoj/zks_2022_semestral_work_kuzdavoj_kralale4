

// visit app landing page
// click on error tab
// assert that the page displays exception

describe('error page', function () {

beforeEach('go to add owner page and add valid owner', function () {
        cy.visit('http://localhost:8080/')
        cy.contains('.nav-item', 'Find owners').click()
        cy.url().should('equal', 'http://localhost:8080/owners/find')
        cy.get('.btn.btn-primary').contains('Add Owner').click()
        cy.url().should('equal', 'http://localhost:8080/owners/new')

        cy.fixture('valid_owner.json').as('validOwner').then(validOwner => {

            // Fill form for new Owner from fixture data
            cy.get('input#firstName').type(validOwner.firstName)
            cy.get('input#lastName').type(validOwner.lastName)
            cy.get('input#address').type(validOwner.address)
            cy.get('input#city').type(validOwner.city)
            cy.get('input#telephone').type(validOwner.telephone)
            // Submit form
            cy.get('button[type=submit]').contains('Add Owner').click()

            cy.get('.container.xd-container')
                .should('contain.text', 'Owner Information')
            cy.get('.table.table-striped')
                .find('tr')
                .should('have.length', 4)


        })
        cy.get('.btn.btn-primary').contains('Add New Pet').click()
        cy.fixture('pets/valid_pet.json').as('validPet').then(validPet => {

            // Fill form for new Pet from fixture data
            cy.get('input#name').type(validPet.petName)
            cy.get('input#birthDate').type(validPet.birthDate)
            cy.get('select').select(validPet.type)

            cy.get('.btn.btn-primary').contains('Add Pet').click()
            cy.get('.table.table-striped')
                .find('dd')
                .first()
                .should('contain.text', validPet.petName)
            cy.get('.table.table-striped')
                .find('dd')
                .last()
                .should('contain.text', validPet.type)
        })

    });
    it('Add a valid visit to a new owner with a new pet', function () {
        cy.fixture('visit_data.json').as('visit').then(visit => {
            cy.get('.table-condensed')
                .find('a')
                .last()
                .click()
            // Fill form for new Pet from fixture data
            cy.get('input#date').type(visit.visitDate)
            cy.get('input#description').type(visit.description)

            cy.get('.btn.btn-primary').contains('Add Visit').click()
            cy.get('.table-condensed')
                .find('tr')
                .eq(1)
                .find('td')
                .last()
                .should('contain.text', visit.description)

        })

    });

    it('Add a visit but forget a description', function () {
        cy.fixture('visit_data.json').as('visit').then(visit => {
            cy.get('.table-condensed')
                .find('a')
                .last()
                .click()
            // Fill form for new Pet from fixture data
            cy.get('input#date').type(visit.visitDate)

            cy.get('.btn.btn-primary').contains('Add Visit').click()
            cy.get('.help-inline').should('contain.text', 'must not be empty')

        })

    });


})
