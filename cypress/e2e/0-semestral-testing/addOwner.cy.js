

// visit app landing page
// click on error tab
// assert that the page displays exception


describe('error page', function () {

    beforeEach('go to add owner page', function () {
        cy.visit('http://localhost:8080/')
        cy.contains('.nav-item', 'Find owners').click()
        cy.url().should('equal', 'http://localhost:8080/owners/find')
        cy.get('.btn.btn-primary').contains('Add Owner').click()
        cy.url().should('equal', 'http://localhost:8080/owners/new')
    });


    it('first name too long/short', function () {
        cy.fixture('invalid_owner_first_name_too_long.json').as('invalidOwner').then(invalidOwner => {

            // Fill form for new Owner from fixture data
            cy.get('input#firstName').type(invalidOwner.firstName)
            cy.get('input#lastName').type(invalidOwner.lastName)
            cy.get('input#address').type(invalidOwner.address)
            cy.get('input#city').type(invalidOwner.city)
            cy.get('input#telephone').type(invalidOwner.telephone)
            // Submit form
            cy.get('button[type=submit]').contains('Add Owner').click()

            // Only one error field
            cy.get('.form-group.has-feedback').find('.has-error').should('have.length', 1)
            // Error field is First Name field
            cy.get('.form-group.has-error').should('contain.text', 'First Name')
            cy.get('.form-group.has-error').find('.help-inline').should('contain.text', 'size must be between 2 and 30')
        })
    });

    it('last name too long/short', function () {
        cy.fixture('invalid_owner_last_name_too_long.json').as('invalidOwner').then(invalidOwner => {

            // Fill form for new Owner from fixture data
            cy.get('input#firstName').type(invalidOwner.firstName)
            cy.get('input#lastName').type(invalidOwner.lastName)
            cy.get('input#address').type(invalidOwner.address)
            cy.get('input#city').type(invalidOwner.city)
            cy.get('input#telephone').type(invalidOwner.telephone)
            // Submit form
            cy.get('button[type=submit]').contains('Add Owner').click()

            // Only one error field
            cy.get('.form-group.has-feedback').find('.has-error').should('have.length', 1)
            // Error field is Last Name field
            cy.get('.form-group.has-error').should('contain.text', 'Last Name')
            cy.get('.form-group.has-error').find('.help-inline').should('contain.text', 'size must be between 2 and 30')
        })
    });

    it('wrong telephone number', function () {
        cy.fixture('invalid_owner_wrong_phone.json').as('invalidOwner').then(invalidOwner => {

            // Fill form for new Owner from fixture data
            cy.get('input#firstName').type(invalidOwner.firstName)
            cy.get('input#lastName').type(invalidOwner.lastName)
            cy.get('input#address').type(invalidOwner.address)
            cy.get('input#city').type(invalidOwner.city)
            cy.get('input#telephone').type(invalidOwner.telephone)
            // Submit form
            cy.get('button[type=submit]').contains('Add Owner').click()

            // Only one error field
            cy.get('.form-group.has-feedback').find('.has-error').should('have.length', 1)
            // Error field is Telephone field
            cy.get('.form-group.has-error').should('contain.text', 'Telephone')
            cy.get('.form-group.has-error').find('.help-inline').should('contain.text', 'numeric value out of bounds (<10 digits>.<0 digits> expected)')
        })
    });

    it('invalid both names and number', function () {
        cy.fixture('invalid_owner_long_names_wrong_phone.json').as('invalidOwner').then(invalidOwner => {

            // Fill form for new Owner from fixture data
            cy.get('input#firstName').type(invalidOwner.firstName)
            cy.get('input#lastName').type(invalidOwner.lastName)
            cy.get('input#address').type(invalidOwner.address)
            cy.get('input#city').type(invalidOwner.city)
            cy.get('input#telephone').type(invalidOwner.telephone)
            // Submit form
            cy.get('button[type=submit]').contains('Add Owner').click()

            // Three error fields
            cy.get('.form-group.has-feedback').find('.has-error').should('have.length', 3)
            // Error field is First Name, Last Name and Telephone field
            cy.get('.form-group.has-error').should('contain.text', 'First Name')
                .find('.help-inline').should('contain.text', 'size must be between 2 and 30')
            cy.get('.form-group.has-error').should('contain.text', 'Last Name')
                .find('.help-inline').should('contain.text', 'size must be between 2 and 30')
            cy.get('.form-group.has-error').should('contain.text', 'Telephone')
                .find('.help-inline').should('contain.text', 'numeric value out of bounds (<10 digits>.<0 digits> expected)')
        })
    });

    it('add valid owner', function () {
        cy.fixture('valid_owner.json').as('validOwner').then(validOwner => {

            // Fill form for new Owner from fixture data
            cy.get('input#firstName').type(validOwner.firstName)
            cy.get('input#lastName').type(validOwner.lastName)
            cy.get('input#address').type(validOwner.address)
            cy.get('input#city').type(validOwner.city)
            cy.get('input#telephone').type(validOwner.telephone)
            // Submit form
            cy.get('button[type=submit]').contains('Add Owner').click()

            cy.get('.container.xd-container')
                .should('contain.text', 'Owner Information')
            cy.get('.table.table-striped')
                .find('tr')
                .should('have.length', 4)
            name = validOwner.firstName + ' ' + validOwner.lastName;
            cy.get('.table.table-striped')
                .first()
                .find('tr')
                .eq(0)
                .find('b')
                .should('contain.text', name)

            cy.get('.table.table-striped')
                .first()
                .find('tr')
                .eq(1)
                .find('td')
                .should('contain.text', validOwner.address)

            cy.get('.table.table-striped')
                .first()
                .find('tr')
                .eq(2)
                .find('td')
                .should('contain.text', validOwner.city)

            cy.get('.table.table-striped')
                .first()
                .find('tr')
                .eq(3)
                .find('td')
                .should('contain.text', validOwner.telephone)


        })
    });

})
