

// visit app landing page
// click on error tab
// assert that the page displays exception

describe('error page', function () {

    beforeEach('Find already existing owner', function () {
        cy.visit('http://localhost:8080/')
        cy.contains('.nav-item', 'Find owners').click()
        cy.url().should('equal', 'http://localhost:8080/owners/find')

        //Here the last could be the last name of any owner to edit

        cy.get('input#lastName').type('Franklin')
        cy.get('.btn.btn-primary').contains('Find Owner').click()
        cy.get('.table.table-striped')
            .should('have.length', 2)

    });


    it('edit valid pet', function () {
        cy.fixture('pets/new_pet_info.json').as('petInfo').then(petInfo => {
            cy.get('.table-condensed')
                .find('a')
                .first()
                .click()

            // Fill form for new Pet from fixture data
            cy.get('input#name')
                .clear()
                .type(petInfo.petName)
            cy.get('input#birthDate').type(petInfo.birthDate)
            cy.get('select').select(petInfo.type)

            cy.get('.btn.btn-primary').contains('Update Pet').click()
            cy.get('.table.table-striped')
                .find('dd')
                .first()
                .should('contain.text', petInfo.petName)
            cy.get('.table.table-striped')
                .find('dd')
                .last()
                .should('contain.text', petInfo.type)
        })

    });

    it('edit valid owner', function () {
            cy.fixture('valid_owner.json').as('validOwner').then(validOwner => {
                cy.get('.btn.btn-primary').contains('Edit Owner').click()

                // Fill form for new Pet from fixture data
                cy.get('input#firstName')
                    .clear()
                    .type(validOwner.firstName)
                cy.get('input#address')
                    .clear()
                    .type(validOwner.address)
                cy.get('input#city')
                    .clear()
                    .type(validOwner.city)
                cy.get('input#telephone')
                    .clear()
                    .type(validOwner.telephone)
                cy.get('.btn.btn-primary').contains('Update Owner').click()
                cy.get('.table.table-striped')
                    .first()
                    .find('tr')
                    .eq(1)
                    .find('td')
                    .should('contain.text', validOwner.address)

                cy.get('.table.table-striped')
                    .first()
                    .find('tr')
                    .eq(2)
                    .find('td')
                    .should('contain.text', validOwner.city)

                cy.get('.table.table-striped')
                    .first()
                    .find('tr')
                    .eq(3)
                    .find('td')
                    .should('contain.text', validOwner.telephone)
            })

        });


})
