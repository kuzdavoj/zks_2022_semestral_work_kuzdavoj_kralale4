

// visit app landing page
// click on error tab
// assert that the page displays exception


describe('error page', function () {
    it('displays the error message', function () {
        cy.visit('http://localhost:8080/')
        cy.contains('.nav-item', 'Error').click()
        cy.url().should('equal', 'http://localhost:8080/oups')

        cy.get('.container.xd-container')
            .should('contain.text', 'Expected: controller used to showcase what happens when an exception is thrown')
            .find('img').should('have.attr', 'src', '/resources/images/pets.png')
    });

})
