

// visit app landing page
// click on Find Owners tab
// find all owners
// find owner Franklin
// try finding non-existing owner


describe('find owners page', function () {

    it('finds all owners', function() {
        cy.visit('http://localhost:8080/')
        cy.contains('.nav-item', 'Find owners').click()

        cy.url().should('contain', '/owners/find')
        cy.get('.container.xd-container')
            .should('contain.text', 'Find Owners')

        cy.get('button[type=submit]').contains('Find Owner').click()

        cy.get('.container.xd-container')
            .should('contain.text', 'Owners')
        cy.get('.table.table-striped')
            .find('tr')
            .should('have.length', 6)
    });

    it('finds the owner Franklin', function() {
        cy.visit('http://localhost:8080/')
        cy.contains('.nav-item', 'Find owners').click()

        cy.url().should('contain', '/owners/find')
        cy.get('.container.xd-container')
            .should('contain.text', 'Find Owners')

        cy.get('#lastNameGroup').type('Franklin')

        cy.get('button[type=submit]').contains('Find Owner').click()

        cy.url().should('contain', '/owners/1')
        cy.get('.container.xd-container')
            .should('contain.text', 'Owner Information')

        // assert Owner table
        cy.get('.table.table-striped')
            .first()
            .find('tr')
            .should('have.length', 4)
    });

    it('finds no user', function() {
        cy.visit('http://localhost:8080/')
        cy.contains('.nav-item', 'Find owners').click()

        cy.url().should('contain', '/owners/find')
        cy.get('.container.xd-container')
            .should('contain.text', 'Find Owners')

        cy.get('#lastNameGroup').type('xxxxxxxx')

        cy.get('button[type=submit]').contains('Find Owner').click()

        cy.contains('.help-inline', 'has not been found')
    });

})
