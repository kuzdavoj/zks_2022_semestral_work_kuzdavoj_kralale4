

// visit app landing page
// click on home tab
// assert that the page displays Welcome message


describe('home page', function () {
    it('displays home page', function () {
        cy.visit('http://localhost:8080/')
        cy.get('.navbar-brand').click()
        cy.url().should('equal', 'http://localhost:8080/')

        cy.get('.container.xd-container')
            .should('contain.text', 'Welcome')
            .find('img').should('have.attr', 'src', '/resources/images/pets.png')
    });
})
