

// visit app landing page
// click on Veterinarians tab
// find all veterinarians


describe('find vets page', function () {

    it('finds all vets', function() {
        cy.visit('http://localhost:8080/')
        cy.contains('.nav-item', 'Veterinarians').click()

        cy.url().should('contain', '/vets.html')

        cy.get('.container.xd-container')
            .should('contain.text', 'Veterinarians')

        cy.get('.table.table-striped')
            .find('tr')
            .should('have.length', 6)

        // visit second page
        cy.get('.fa.fa-step-forward').click()
        cy.get('.table.table-striped')
            .find('tr')
            .should('have.length', 2)
    })
})
