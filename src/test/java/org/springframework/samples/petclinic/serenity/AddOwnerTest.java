package org.springframework.samples.petclinic.serenity;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Managed;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.samples.petclinic.serenity.pageObjects.petClinicPage;
import org.springframework.samples.petclinic.serenity.tasks.AddOwner;

import java.util.ArrayList;
import java.util.List;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.springframework.samples.petclinic.serenity.questions.OwnerFullName.theDisplayedName;


//@RunWith(SerenityRunner.class)
@SpringBootTest(
		classes = {
				org.springframework.samples.petclinic.PetClinicApplication.class
		},
		webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT
)
public class AddOwnerTest {

	private final Actor james = new Actor("James");

	petClinicPage mainPage;

	@Managed
	WebDriver serenityDriver;

	@BeforeEach
	public void before() {
		givenThat(james).can(BrowseTheWeb.with(serenityDriver));
	}

	@Test
	public void actor_should_be_able_to_find_default_owner() {


		List<String> credentials = new ArrayList<>();
		//First name
		credentials.add("Mike");
		//Last name
		credentials.add("Chubby");
		//Address
		credentials.add("At meat street 42");
		//City
		credentials.add("Wellington");
		//Telephone
		credentials.add("456987222");
		givenThat(james).wasAbleTo(Open.browserOn(mainPage));
		when(james).attemptsTo(AddOwner.called(credentials));
		then(james).should(seeThat(theDisplayedName(), hasItems(credentials.get(0)+ " " + credentials.get(1))));

	}

}
