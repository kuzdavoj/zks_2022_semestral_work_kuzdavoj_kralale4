package org.springframework.samples.petclinic.serenity;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import org.springframework.samples.petclinic.serenity.tasks.FindOwner;
import net.thucydides.core.annotations.Managed;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.springframework.boot.test.context.SpringBootTest;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.samples.petclinic.serenity.questions.OwnerFullName.theDisplayedName;
import org.springframework.samples.petclinic.serenity.pageObjects.petClinicPage;


//@RunWith(SerenityRunner.class)
@SpringBootTest(
		classes = {
				org.springframework.samples.petclinic.PetClinicApplication.class
		},
		webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT
)
public class FindOwnerTest {

	private final Actor james = new Actor("James");

	petClinicPage mainPage;

	@Managed
	WebDriver serenityDriver;

	@BeforeEach
	public void before() {
		givenThat(james).can(BrowseTheWeb.with(serenityDriver));
	}

	@Test
	public void actor_should_be_able_to_find_default_owner() {
		String ownerFirstName = "George";
		String ownerLastName = "Franklin";
		String ownerFullName = ownerFirstName + " " + ownerLastName;
		givenThat(james).wasAbleTo(Open.browserOn(mainPage));
		when(james).attemptsTo(FindOwner.called(ownerLastName));
		then(james).should(seeThat(theDisplayedName(), hasItems(ownerFullName)));

	}

}
