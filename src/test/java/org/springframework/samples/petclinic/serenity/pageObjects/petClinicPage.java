package org.springframework.samples.petclinic.serenity.pageObjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.serenitybdd.screenplay.ui.Button;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://localhost:8080/")
public class petClinicPage extends PageObject {


	public static Target HOME = Target.the("Home")
		.locatedBy(".fa.fa-home");
	public static Target FIND_OWNER;
	public static Target VETERINARIANS;
	public static Target ERROR;
	public static final Target OWNER_CATEGORY = Target
		.the("Open find owner page")
		.locatedBy("//*[@title='find owners']");

	public static final Target OWNER_INPUT_FIELD = Target
		.the("Locate input field")
		.locatedBy("//*[@id='lastName']");

	public static final Target FIND_OWNER_BUTTON = Target
		.the("Locate find owner button")
		.locatedBy("//*[@type='submit']");

	public static final Target ADD_OWNER_BUTTON = Target
		.the("Locate add owner button")
		.locatedBy("//*[contains(text(),'Add Owner')]");

	public static final Target FIRST_NAME_INPUT = Target
		.the("Input first name")
		.locatedBy("//*[@id='firstName']");

	public static final Target LAST_NAME_INPUT = Target
		.the("Input last name")
		.locatedBy("//*[@id='lastName']");

	public static final Target ADDRESS_INPUT = Target
		.the("Input address")
		.locatedBy("//*[@id='address']");

	public static final Target CITY_INPUT = Target
		.the("Input city")
		.locatedBy("//*[@id='city']");

	public static final Target TELEPHONE_INPUT = Target
		.the("Input telephone")
		.locatedBy("//*[@id='telephone']");

	public static final Target SHOW_FULL_NAME = Target
		.the("Show owners full name")
		.locatedBy(".table.table-striped td");
}
