package org.springframework.samples.petclinic.serenity.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import org.springframework.samples.petclinic.serenity.pageObjects.petClinicPage;

import java.util.List;


public class OwnerFullName implements Question<List<String>> {

    @Override
    public List<String> answeredBy(Actor actor) {

        return (List<String>) Text.ofEach(petClinicPage.SHOW_FULL_NAME).answeredBy(actor);
    }

    public static Question<List<String>> theDisplayedName() {
        return new OwnerFullName();
    }

}
