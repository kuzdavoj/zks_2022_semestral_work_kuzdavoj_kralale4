package org.springframework.samples.petclinic.serenity.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.springframework.samples.petclinic.serenity.pageObjects.petClinicPage;

//import static todomvc.pageobjects.TodoMVCPage.NEW_TODO_FIELD;
;import java.util.List;

public class AddOwner implements Task {

    private final List<String> credentials;

    protected AddOwner(List<String> credentials) {
        this.credentials = credentials;
    }

    public static AddOwner called(List<String> credentials) {
        return Instrumented.instanceOf(AddOwner.class)
                .withProperties(credentials);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(petClinicPage.OWNER_CATEGORY),
				Click.on(petClinicPage.ADD_OWNER_BUTTON),
				Enter.theValue(credentials.get(0)).into(petClinicPage.FIRST_NAME_INPUT),
				Enter.theValue(credentials.get(1)).into(petClinicPage.LAST_NAME_INPUT),
				Enter.theValue(credentials.get(2)).into(petClinicPage.ADDRESS_INPUT),
				Enter.theValue(credentials.get(3)).into(petClinicPage.CITY_INPUT),
				Enter.theValue(credentials.get(4)).into(petClinicPage.TELEPHONE_INPUT),
				Click.on(petClinicPage.ADD_OWNER_BUTTON)
        );
    }
}
