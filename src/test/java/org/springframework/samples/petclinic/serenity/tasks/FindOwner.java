package org.springframework.samples.petclinic.serenity.tasks;

import net.serenitybdd.screenplay.actions.Enter;
import org.springframework.samples.petclinic.serenity.pageObjects.petClinicPage;
import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

//import static todomvc.pageobjects.TodoMVCPage.NEW_TODO_FIELD;
;

public class FindOwner implements Task {

    private final String ownerName;

    protected FindOwner(String ownerName) {
        this.ownerName = ownerName;
    }

    public static FindOwner called(String categoryName) {
        return Instrumented.instanceOf(FindOwner.class)
                .withProperties(categoryName);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(petClinicPage.OWNER_CATEGORY),
				Enter.theValue(ownerName).into(petClinicPage.OWNER_INPUT_FIELD),
				Click.on(petClinicPage.FIND_OWNER_BUTTON)
        );
    }
}
